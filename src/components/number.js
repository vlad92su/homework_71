import React from 'react';

const Number = props => {
    return (
        <div className="circle">
           <div className="number">{props.number}</div>
        </div>
    );
}

export default Number;

